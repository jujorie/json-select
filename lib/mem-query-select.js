'use strict';

/**
 * Selects elements of a object
 * @param {object} source - Object to extract data
 * @param {string|Array<string>} exprs - Select expressions
 * @return {Object}
 */
function memQuerySelect(source, exprs) {
    let selects;
    const result = {};
    if (typeof exprs === 'string') {
        selects = [exprs];
    } else {
        selects = exprs;
    }

    selects.forEach((select) => {
        const parts = select.split('.');
        let part = result;
        let value = source;
        for (let i = 0; i < parts.length; i++) {
            const key = parts[i];
            if (value[key]) {
                if (!part[key]) {
                    part[key] = {};
                }
                if (i === parts.length - 1) {
                    part[key] = value[key];
                }
            }
            part = part[key];
            value = value[key];
        }
    });
    return result;
}

module.exports = memQuerySelect;
