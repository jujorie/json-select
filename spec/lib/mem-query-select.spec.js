'use script';

describe('mem-query-select', () => {
    const memQuerySelect = require('../../lib/mem-query-select');
    const testCases = require('./mem.query-select.testcases');

    testCases.testCases.forEach((testCase) => {
        it(`Must select ${JSON.stringify(testCase.expr)}`, () => {
            const result = memQuerySelect(testCases.context, testCase.expr);
            expect(result).toEqual(testCase.expected);
        });
    });
});
