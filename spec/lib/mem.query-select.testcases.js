module.exports = {
    context: {
        a: {
            c: {
                i: {},
                j: 'c',
                k: 2
            },
            d: {
                l: 3,
                m: {
                    p: {
                        q: {
                            r: 5,
                            s: 'e'
                        }
                    }
                }
            },
            e: 1,
            f: 'a'
        },
        b: {
            g: {
                n: 4,
                o: 'd'
            },
            h: 'b'
        },
        t: 6,
        u: 'f'
    },

    testCases: [
        {
            expr: ['a.d.m.p.q.r'],
            expected: {
                a: {d: {m: {p: {q: {r: 5}}}}}
            }
        },
        {
            expr: 'a.d.m.p.q.r',
            expected: {
                a: {d: {m: {p: {q: {r: 5}}}}}
            }
        },
        {
            expr: [
                'a.d.m.p.q.r',
                'a.d.m.p.q.s',
            ],
            expected: {
                a: {d: {m: {p: {q: {r: 5, s: 'e'}}}}}
            }
        },
        {
            expr: [
                'a.c.k',
                'a.d.m.p.q.s',
            ],
            expected: {
                a: {c: {k: 2}, d: {m: {p: {q: {s: 'e'}}}}}
            }
        },
        {
            expr: 'b',
            expected: {
                b: {
                    g: {
                        n: 4,
                        o: 'd'
                    },
                    h: 'b'
                }
            }
        },
        {
            expr: [
                't',
                'u',
            ],
            expected: {t: 6, u: 'f'}
        },
        {
            expr: 'a.c.z',
            expected: {a: {c: {}}}
        },
        {
            expr: 'a.c.i',
            expected: {a: {c: {i: {}}}}
        }
    ]
};
